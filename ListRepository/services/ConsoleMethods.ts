class ConsoleMethods {
    private greenColor = '\x1b[32m%s\x1b[0m'
    private blueColor = '\x1b[36m%s\x1b[0m'
    private redColor = '\x1b[31m%s\x1b[0m'
    public readRepository() {
        console.log(this.blueColor, 'READING REPOSITORY');
    }
    public errorLog(err: Error) {
        console.log(this.redColor, 'SOMETHING WENT WRONG');
        console.log(err);
    }
    public finishScript() {
        console.log(this.greenColor, 'FILE CREATED');
    }
}
const consoleInstance = new ConsoleMethods
export { consoleInstance as ConsoleMethods } 