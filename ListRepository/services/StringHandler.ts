class StringHandler {
    public getRawName(string: string) {
        return string.split('.svg')[0];
    }
    public isYamlFile(string: string) {
        return string.substr(string.length - 4) === 'yaml';
    }
}
const stringHandlerInstance = new StringHandler
export { stringHandlerInstance as StringHandler }