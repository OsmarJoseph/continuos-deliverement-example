import { readFileSync, writeFileSync, existsSync, readdirSync, mkdirSync } from "fs";
import { safeLoad, safeDump } from "js-yaml";
import { StringHandler } from "./StringHandler";
import { IconsPathsInterface, IconInterface } from "types";

class FileSystemMethods {
    private repositoryTreePath = `./repository-tree.yaml`
    private iconsFolderPath = `./files`


    public getMetadata (path: string) {
        const yamlFile = readFileSync(path, 'utf8');
        const metadata = safeLoad(yamlFile) as object;
        return metadata;
    }

    public createFile (AllPathsList: IconsPathsInterface) {
        let yamlString = safeDump(AllPathsList);
        writeFileSync(this.repositoryTreePath, yamlString, 'utf8');
    }

    public readRepository () {
        const { iconsFolderPath, getMetadata } = this
        const AllPathsList = {} as IconsPathsInterface
        if (!existsSync(iconsFolderPath)) {
            mkdirSync(iconsFolderPath)
        }
        readdirSync(iconsFolderPath).forEach((folder) => {
            AllPathsList[folder] = { icons: [], metadata: {} }

            const iconsList = [] as Array<IconInterface>
            readdirSync(`${iconsFolderPath}/${folder}`)
                .forEach((file) => {
                    const newFilePath = `${iconsFolderPath}/${folder}/${file}`;
                    if (StringHandler.isYamlFile(file)) {
                        const metadata = getMetadata(newFilePath);
                        if (Object.keys(metadata).length) {
                            AllPathsList[folder].metadata = metadata;
                        }
                    } else {
                        const rawName = StringHandler.getRawName(file);
                        if (rawName !== '.DS_Store')
                            iconsList.push({ path: newFilePath, rawName })
                    }
                })
            AllPathsList[folder].icons = iconsList
        });
        return AllPathsList
    }
}
const FileSystemMethodsInstance = new FileSystemMethods()
export { FileSystemMethodsInstance as FileSystemMethods }