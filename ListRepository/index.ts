import { ConsoleMethods } from './services/ConsoleMethods';
import { FileSystemMethods } from './services/FileSystemMethods';

function readRepository() {
    try {
        ConsoleMethods.readRepository()

        const AllPathsList = FileSystemMethods.readRepository()
        FileSystemMethods.createFile(AllPathsList);

        ConsoleMethods.finishScript()

    } catch (err) {
        ConsoleMethods.errorLog(err);
    }
}
readRepository();



