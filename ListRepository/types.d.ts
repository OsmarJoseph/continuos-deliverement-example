export interface IconInterface {
    path: string,
    rawName: string
}
interface PathInterface {
    icons: Array<IconInterface>,
    metadata: object
}
export interface IconsPathsInterface {
    [key: string]: PathInterface
}